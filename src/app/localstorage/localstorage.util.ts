export function setStorage<T>(key: string, value: T): void {
	const json: string = JSON.stringify(value);
	const encoded: string = btoa(json);
	localStorage.setItem(key, encoded);
}
export function getStorage<T>(key: string): T | null {
	const saved: string | null = localStorage.getItem(key);
	if (!saved) {
		return null
	}
	const decoded: string = atob(saved);
	const data: T = JSON.parse(decoded) as T;
	return data;
}