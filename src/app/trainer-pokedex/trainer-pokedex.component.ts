import { Component, OnInit } from '@angular/core'
import { Router } from '@angular/router'
import { Pokemon } from '../models/pokemons.model'
import { CatchPokemonService } from '../services/catch-pokemon.service'

@Component({
    selector: 'app-trainer-pokedex',
    templateUrl: './trainer-pokedex.component.html',
    styleUrls: ['./trainer-pokedex.component.css']
})

export class TrainerPokedexComponent implements OnInit {
    constructor(
        private readonly catchPokemonService: CatchPokemonService,
        private readonly router: Router
    ) { }

    ngOnInit(): void {
        this.catchPokemonService.loadCaughtPokemon();
      }
    
    get pokemon(): Pokemon[] {
      return this.catchPokemonService.collectedPokemon();
    }
  
    onCatchFirstPokemon() {
      this.router.navigate(['/pokemons']);
    }
}