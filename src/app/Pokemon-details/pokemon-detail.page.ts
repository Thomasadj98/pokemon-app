import { Component, OnInit } from '@angular/core'
import { ActivatedRoute } from '@angular/router';
import { Pokemon } from '../models/pokemons.model';
import { PokemonsService } from '../services/pokemons.service'
import { CatchPokemonService } from '../services/catch-pokemon.service'
import { LoadingService } from '../services/loading.service';

@Component({
    selector: 'app-pokemon-details',
    templateUrl: './pokemon-detail.page.html',
    styleUrls: ['./pokemon-detail.page.css']
})

export class PokemonDetailsPage implements OnInit {

    constructor(
        private route: ActivatedRoute,
        private readonly pokemonsService: PokemonsService,
        private readonly catchPokemonService: CatchPokemonService,
        private readonly loadingService: LoadingService
    ) { }

    ngOnInit(): void {
        this.route.params.subscribe(({ id }) => {
            this.pokemonsService.fetchPokemonDetailsById(id);
            this.catchPokemonService.loadCaughtPokemon();
        })
    }

    get pokemon(): Pokemon | undefined {
        return this.pokemonsService.selectedPokemon();
    }    

    get error(): string {
        return this.pokemonsService.error();
    }

    get loading(): boolean {
        return this.loadingService.loading();
      }    

    get isCollected(): boolean {
        return this.catchPokemonService.isCollected(this.pokemon?.name);
      }
    
      onCollectClick(): void {
        this.catchPokemonService.collectPokemon(this.pokemon);
      }
    
      onLetGoClick(): void {
        this.catchPokemonService.letGoPokemon(this.pokemon?.name);
      }
    
    
}