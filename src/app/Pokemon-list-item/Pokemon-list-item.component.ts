import { Component, Input, Output, EventEmitter, OnInit } from '@angular/core';
import { Pokemon } from '../models/pokemons.model';
import { ActivatedRoute, Router } from '@angular/router'
import { SelectedPokemonService } from '../services/selected-pokemon.service';

@Component({
    selector: 'app-pokemon-list-item',
    templateUrl: './pokemon-list-item.component.html',
    styleUrls: ['./pokemon-list-item.component.css']
})

export class PokemonListItemComponent implements OnInit {
    @Input() pokemon: Pokemon | undefined;
    pokemonImage: string = '';
    pokemonId: number = -1;
    pokemonName: string = '';

    constructor(
        // private readonly selectedPokemonService: SelectedPokemonService,
        private router : Router
    ) { }

    ngOnInit(): void {
        const details = this.getIdAndImage(this.pokemon?.url || '')
        this.pokemonImage = details.image;
        this.pokemonId = details.id;
        this.pokemonName = this.pokemon?.name || '';
      }    

    onPokemonClick() {
        this.router.navigate(['pokemons', this.pokemonId]);
      }

      private getIdAndImage(url: string): any {    
        if (url === '') return;
    
        const id = url.split('/').filter(Boolean).pop();
        return { id, image: `https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/${id}.png` };
      }
    
}