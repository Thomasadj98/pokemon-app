export interface Trainer {
    name: string,
    isLoggedIn: boolean
}