import { Component } from '@angular/core'
import { FormGroup, NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { Trainer } from '../models/trainer.model';
import { TrainerLogin } from '../services/trainer-login.service';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.css']
})

export class LoginComponent {

    constructor(
        private readonly trainerlogin: TrainerLogin,
        private readonly router: Router
    ) { } 

    public onSubmit(createForm: NgForm): void {
        console.log(createForm.value.name);
        this.trainerlogin.login(createForm.value.name)
        this.router.navigate(['./trainer'])
    }

}