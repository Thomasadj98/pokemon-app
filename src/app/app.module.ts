// Modules

import { FormsModule } from '@angular/forms';
import { AppRoutingModule } from './services/app-routing.module';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http'

// Components & Pages

import { AppComponent } from './app.component';
import { PokemonComponent } from './Pokemon-list/pokemon.component';
import { PokemonListItemComponent } from './Pokemon-list-item/Pokemon-list-item.component';
import { TrainerCreatePage } from './trainer-create/trainer-create.page';
import { PokemonsPage } from './Pokemons/pokemons.page';
import { PokemonDetailsPage } from './Pokemon-details/pokemon-detail.page';
import { IntroPage } from './intro/intro.page';
import { LoginComponent } from './login/login.component';
import { TrainerPage } from './trainer/trainer.page';
import { TrainerPokedexComponent } from './trainer-pokedex/trainer-pokedex.component';


@NgModule({
  declarations: [
    AppComponent,
    PokemonComponent,
    PokemonListItemComponent,
    PokemonsPage,
    TrainerCreatePage,
    PokemonDetailsPage,
    IntroPage,
    LoginComponent,
    TrainerPage,
    TrainerPokedexComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
