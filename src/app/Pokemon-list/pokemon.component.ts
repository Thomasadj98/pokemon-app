import { Component, OnInit } from '@angular/core'
import { Pokemon } from '../models/pokemons.model';
import { PokemonsService } from '../services/pokemons.service';
import { SelectedPokemonService } from '../services/selected-pokemon.service';

@Component({
    selector: 'app-pokemon',
    templateUrl: './pokemon.component.html',
    styleUrls: ['./pokemon.component.css']
})

export class PokemonComponent implements OnInit {
    constructor(
        private readonly pokemonsService: PokemonsService,
        private readonly SelectedPokemonService: SelectedPokemonService
        ){
    }

    ngOnInit(): void {
        this.pokemonsService.fetchPokemons();
    }

    get pokemons(): Pokemon[] {
        return this.pokemonsService.pokemons();
    }

    public handlePokemonClicked(pokemon: Pokemon): void {
        console.log("click");
        this.SelectedPokemonService.setPokemon(pokemon);
    }
}