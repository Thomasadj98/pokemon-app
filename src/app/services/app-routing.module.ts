import { NgModule } from '@angular/core'
import { RouterModule, Routes } from '@angular/router'
import { IntroPage } from '../intro/intro.page'
import { PokemonDetailsPage } from '../Pokemon-details/pokemon-detail.page'
import { PokemonsPage } from '../Pokemons/pokemons.page'
import { TrainerPage } from '../trainer/trainer.page'

// Pokemons
// Create Pokemon / Trainer

const routes: Routes = [
    {
        path: '',
        pathMatch: 'full',
        redirectTo: '/intro'
    },
    {
        path: 'pokemons',
        component: PokemonsPage
    },
    {
        path: 'pokemons/:id',
        component: PokemonDetailsPage
    },
    {
        path: 'intro',
        component: IntroPage
    },
    {
        path: 'trainer',
        component: TrainerPage
    }
]

@NgModule({
    imports: [ RouterModule.forRoot(routes) ],
    exports: [ RouterModule ]
})

export class AppRoutingModule {

}