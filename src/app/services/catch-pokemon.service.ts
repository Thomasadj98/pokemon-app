import { Injectable } from '@angular/core'
import { Pokemon } from '../models/pokemons.model'
import { setStorage, getStorage } from '../localstorage/localstorage.util'

@Injectable({
    providedIn: 'root'
})

export class CatchPokemonService {
    private _collectedPokemon: Pokemon[] = [];

  constructor() { }

  public collectPokemon(pokemon: Pokemon | undefined): void {
    if (pokemon === undefined) return

    const { name, url } = pokemon;

    this._collectedPokemon.push(pokemon);
    setStorage<Pokemon[]>('collected-pokemon', this._collectedPokemon);
  }

  public loadCaughtPokemon(): Pokemon[] {
    const storedPokemon = getStorage<Pokemon[]>('collected-pokemon');

    if (storedPokemon) {
      this._collectedPokemon = storedPokemon;
    }

    return this._collectedPokemon;
  }

  public letGoPokemon(name: string | undefined): void {
    if(name === undefined) return;

    this._collectedPokemon = this._collectedPokemon.filter((pokemon: Pokemon) => {
      return pokemon.name !== name
    })

    setStorage<Pokemon[]>('collected-pokemon', this._collectedPokemon);
  }

  public collectedPokemon(): Pokemon[] {
    return this._collectedPokemon;
  }

  public isCollected(name: string | undefined): boolean {
    if (name === undefined) return false;
    return this._collectedPokemon.some((pokemon: Pokemon) => pokemon.name === name);
  }

}