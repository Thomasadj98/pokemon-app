import { Injectable } from '@angular/core'
import { Trainer } from '../models/trainer.model'
import { setStorage, getStorage } from '../localstorage/localstorage.util'

@Injectable({
    providedIn: 'root'
})

export class TrainerLogin {

    private _trainer: Trainer = {
        name: '',
        isLoggedIn: false
      };
    
    constructor() { }

    public login(name: string): void {
        this._trainer = {
          name,
          isLoggedIn: true
        },
        setStorage<string>('trainer-name', name);
      }

      public logout(): void {
        this._trainer = {
          name: '',
          isLoggedIn: false
        }
        localStorage.clear();
      }
    
      public isLoggedIn(): boolean {
        if (this._trainer?.isLoggedIn) return true;
    
        const user = getStorage<string>('trainer-name');
    
        if (user) {
          this.login(user);
          return true;
        }
    
        return false;
      }
    
      public trainer(): Trainer {
        return this._trainer;
      }    

}