import { HttpClient, HttpErrorResponse } from '@angular/common/http'
import { Injectable } from '@angular/core'
import { Pokemon } from '../models/pokemons.model'
import { map } from 'rxjs/operators'
import { LoadingService } from './loading.service'

@Injectable({
    providedIn: 'root'
})

export class PokemonsService {
    private _pokemons: Pokemon[] = [];
    private _error: string = '';
    private _selectedPokemon: Pokemon | undefined;

    constructor(
        private readonly http: HttpClient,
        private readonly loadingService: LoadingService
        ) { }

    public fetchPokemons(): void {
        // Observables
        this.http.get<Pokemon[]>('http://localhost:3000/pokemons')
            .pipe(
                map((response: any) => {
                return response.results as Pokemon[]
                })
            )
            .subscribe(pokemons => {
                this._pokemons = pokemons.map(pokemon => {
                    return {...pokemon, ...this.getIdAndImage(pokemon.url)}
                });
            }, (error: HttpErrorResponse) => {
                this._error = error.message;
            });
    }

    private getIdAndImage(url: string): any {
        const id = url.split( '/' ).filter( Boolean ).pop();
        return { id, image: `https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/${ id }.png`};
    }

    public fetchPokemonDetailsById(id: number): void {
        this.loadingService.setLoading(true);

        this._selectedPokemon = this._pokemons.find((pokemon: Pokemon) => pokemon.id === id);

        if (this._selectedPokemon) {
            this.loadingService.setLoading(false);
            return;
        }

        const url = `https://pokeapi.co/api/v2/pokemon/${id}`;

        this.http.get<Pokemon>(url)
            .subscribe((pokemon: Pokemon) => {
                pokemon.url = url;
                this.addPokemonToSavedPokemons(pokemon);
                this._selectedPokemon = pokemon;
                this.loadingService.setLoading(false);
            }, (error: HttpErrorResponse) => {
                this._error = error.message;
                this.loadingService.setLoading(false);
            })
    }

    
    private addPokemonToSavedPokemons(pokemon: Pokemon): void {
        this._pokemons.push(pokemon);
    }

    public pokemons(): Pokemon[] {
        return this._pokemons;
    }

    public selectedPokemon(): Pokemon | undefined {
        return this._selectedPokemon;
    }

    public error(): string {
        return this._error;
    }

}